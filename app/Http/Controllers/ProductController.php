<?php

namespace App\Http\Controllers;

use App\Models\ShopifyImage;
use Illuminate\Http\Request;
use App\Models\ShopifyOption;
use App\Models\ShopifyProduct;
use App\Models\ShopifyVariant;
use App\Models\BusinessSetting;
use App\Models\BusinessWebhook;
use App\Models\WebhookResponse;
use App\Models\ShopifyOptionType;

class ProductController extends Controller
{
  public function WebhookResponse(Request $request,$id)
  {
      $input=$request->all();

      $url=$_SERVER['REQUEST_URI'];


       $explode=explode("/",$url);
       $webhook_type=$explode[4]."/".$explode[5];
   
       $business_webhook=BusinessWebhook::where(['user_id'=>$id,'webhook_type'=>$webhook_type])->first();


      $response_store=WebhookResponse::create([
          'user_id'=>$id,
          'webhook_id'=>$business_webhook->webhook_id,
          'webhook_type'=>$webhook_type,
          'payload'=>json_encode($input),
          'status'=>0
      ]);


      return response()->json(['code'=>200,"message"=>"Success"]);
      // $this->storeOrUpdate();
    // user_id	webhook_id	webhook_type	payload	status
  }


  public function storeOrUpdate()
  {

    $get_data=WebhookResponse::where([
       'status'=>0,
       'webhook_type'=>'products/update'
    ])->get();
    foreach($get_data as $data){

      $domain = BusinessSetting::where('user_id', $data->user_id)->first();

    $shop_link = $domain->store_link;

    // $products = products(['url' => $shop_link, 'access_token' => $domain->access_token]);
    // echo '<div style="padding-top:100px;"><center><h3>We are Fetching products from Your Shopify Shop</h3><br><img src="https://www.nomadskybar.ro/wp-content/uploads/AAPL/loaders/loading-nomad.gif" height="50px"></center></div>';

    $product=json_decode($data->payload);

      if ($product->options[0]->name == 'Title') {
        $has_varient = false;
        $option_data = [
          'shopify_option_id' => $product->options[0]->id,
          'product_id' => $product->options[0]->product_id,
          'title' => $product->options[0]->name,
          'position' => $product->options[0]->position
        ];
        $adding_op = ShopifyOption::updateOrCreate(['product_id' => $product->options[0]->product_id, 'position' => $product->options[0]->position], $option_data);
      } else {
        $has_varient = true;

        foreach ($product->options as $op) {
          $option_data = [
            'shopify_option_id' => $op->id,
            'product_id' => $op->product_id,
            'title' => $op->name,
            'position' => $op->position
          ];
          $adding_op = ShopifyOption::updateOrCreate(['product_id' => $op->product_id, 'position' => $op->position], $option_data);

          foreach ($op->values as $val) {
            $op_type_data = [
              'option_id' => $adding_op->id,
              'product_id' => $op->product_id,
              'title' => $val
            ];

            $add_ty_op = ShopifyOptionType::updateOrCreate(['option_id' => $adding_op->id, 'product_id' => $op->product_id, 'title' => $val], $op_type_data);
          }
        }
      }
      $data_product = [
        'shopify_product_id' => $product->id,
        'company_id' => $domain->company_id,
        'title' => $product->title,
        'description' => $product->body_html,
        'vendor' => $product->vendor,
        'product_type' => $product->product_type,
        'publish_at' => $product->published_at,
        'tamplate_suffix' => $product->template_suffix,
        'status' => $product->status,
        'published_scope' => $product->published_scope,
        'tag' => $product->tags,
        'admin_graph_api_id' => $product->admin_graphql_api_id,
        'image' => $product->image->src,
        'has_varients' => $has_varient
      ];

      $product_create = ShopifyProduct::updateOrCreate(['shopify_product_id' => $product->id, 'company_id' => $domain->company_id,], $data_product);
      foreach ($product->variants as $vari) {
        $varient_data = [
          'veriant_id' => $vari->id,
          'product_id' => $vari->product_id,
          'title' => $vari->title,
          'price' => $vari->price,
          'compare_at_price' => $vari->compare_at_price,
          'sku' => $vari->sku,
          'position' => $vari->position,
          'inventory_policy' => $vari->inventory_policy,
          'fulfillment_service' => $vari->fulfillment_service,
          'inventory_management' => $vari->inventory_management,
          'option1' => $vari->option1,
          'option2' => $vari->option2,
          'option3' => $vari->option3,
          'taxable' => $vari->taxable,
          'barcode' => $vari->barcode,
          'image_id' => $vari->image_id,
          'weight' => $vari->weight,
          'weight_unit' => $vari->weight_unit,
          'inventory_item_id' => $vari->inventory_item_id,
          'old_inventory_quantity' => $vari->old_inventory_quantity,
          'inventory_quantity' => $vari->inventory_quantity,
          'require_shipping' => $vari->requires_shipping,
          'admin_graphql_api_id' => $vari->admin_graphql_api_id,
          'sync_at' => time()
        ];
        $variants = ShopifyVariant::updateOrCreate(['veriant_id' => $vari->id, 'product_id' => $vari->product_id,], $varient_data);
      }

      foreach ($product->images as $img) {
        $img_data = [
          'product_id' => $img->product_id,
          'image_id' => $img->id,
          'position' => $img->position,
          'src' => $img->src,
          'alt' => $img->alt,
          'variant_id' => json_encode($img->variant_ids),
          'width' => $img->width,
          'height' => $img->height
        ];

        $add_img = ShopifyImage::updateOrCreate(['product_id' => $img->product_id, 'position' => $img->position,], $img_data);
      }
      $update_webhook=WebhookResponse::where([
            'id'=>$data->id
            ])->update([
              'status'=>1,
           ])->get();
     }

    // dd($get_data);
    return response()->json(json_decode($data->payload));
  }
}
