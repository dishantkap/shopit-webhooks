<?php

namespace App\Http\Controllers;

use App\Models\BusinessSetting;
use App\Models\BusinessWebhook;
use App\Models\ShopitWebhook;
use App\Models\User;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
  public function index( Request $request, $user_id)
  {
     $user=User::where('id',$user_id)->first();

     $settings=BusinessSetting::where('user_id',$user_id)->first();
     $webhooks=ShopitWebhook::get();
     $response = array();

     $company_hooks=list_webhooks(['company_id'=>$settings->company_id]);
    if(isset($company_hooks->webhooks)){
      foreach($company_hooks->webhooks as $hook){
        if(isset($hook->webhook->id)){
          $company_hooks=remove_webhooks(['company_id'=>$settings->company_id,'webhook_id'=>$hook->webhook->id]); 
        }else{
          if(isset($hook->id)){
            $company_hooks=remove_webhooks(['company_id'=>$settings->company_id,'webhook_id'=>$hook->id]); 

          }
        }
      }
    }
    //  dd($settings->toArray());
     foreach($webhooks as $hooks){
       $hook=  create_hook(['company_id'=>$settings->company_id,'topic'=>$hooks->webhook_type,'url'=>$hooks->address."/".$user_id]);
       $response[] = $hook;
      //  dd($hook['webhook']['id']);
      //  if($hook!=null && isset($hook->webhook)){
          $create=BusinessWebhook::create([
              'user_id'=>$user_id,
              'webhook_type'=>$hooks->webhook_type,
              'webhook_endpoint'=>$hooks->address."/".$user_id,
              'company_profile_id'=>$settings->company_id,
              'webhook_id'=>(isset($hook['webhook']['id']))?$hook['webhook']['id']:0
          ]);
      // }
    }
    return json_encode($response);
  }

  // public function update_hook(Request $request, $user_id)
  // {
  //   $user=User::where('id',$user_id)->get();
  //   $settings=BusinessSetting::where('user_id',$user_id)->first();
  //   $webhooks=BusinessWebhook::get();

  // }

  public function list_webhooks(Request $request, $id)
  {

    $settings=BusinessSetting::where('user_id',$id)->first();
    $company_hooks=list_webhooks(['company_id'=>$settings->company_id]);
    return response()->json($company_hooks->webhooks);
  }
 

  public function remove_webhooks(Request $request, $id,$w_id)
  {

    // dd( $id,$w_id);
    $settings=BusinessSetting::where('user_id',$id)->first();
    $company_hooks=remove_webhooks(['company_id'=>$settings->company_id,'webhook_id'=>$w_id]);
    return response()->json($company_hooks);
  }

  public function remove_all_webhooks(Request $request, $user_id)
  {

    $settings=BusinessSetting::where('user_id',$user_id)->first();
    $all_hooks=BusinessWebhook::where('user_id',$user_id)->get();
  
    foreach($all_hooks as $hook){
      $company_hooks=remove_webhooks(['company_id'=>$settings->company_id,'webhook_id'=>$hook->webhook_id]);
    }
    $all_hooks=BusinessWebhook::where('user_id',$user_id)->get();
    $delete_all_hooks=BusinessWebhook::where('user_id',$user_id)->delete();
    return response()->json(["code"=>200,"result"=>$all_hooks]);
    // dd($company_hooks);
  }
}
