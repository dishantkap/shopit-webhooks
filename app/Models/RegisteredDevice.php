<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegisteredDevice extends Model
{
  use SoftDeletes;
  protected $fillable = [
    'user_id',
     'device_id',
     'device_token',
     ];

     
}
