<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $fillable=[
         'user_id',
         'business_id',
         'checkout_token',
         'payment_link',
         'order_number',
         'payment_status',
         'order_status',
         'order_id',
         'amount',
         'unique_token',
         'checkout_id',
         'currency',
         'api_payment_url',
         'shipping_carrier_id',
         'tracking_number'
    ];

    public function company()
    {
        return $this->hasOne('App\Models\CompanyProfile', 'id', 'business_id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
