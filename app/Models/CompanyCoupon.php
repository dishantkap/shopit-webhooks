<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyCoupon extends Model
{
   use SoftDeletes;

    protected $table = 'company_coupons';
    protected $fillable = ['company_id', 'name', 'code', 'type', 'discount', 'limit', 'currency_id', 'start_from', 'end_on', 'description','for_first_time'];
    protected $dates = [
        'start_from',
        'end_on',
        'created_at',
        'delete_at',
        'updated_at'
    ];
    public function currency()
    {
        return $this->hasOne('App\Models\Currency', 'id', 'currency_id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\CompanyProfile', 'id', 'company_id');
    }

    public function services()
    {
        return $this->hasMany('App\Models\CouponService', 'coupon_id', 'id');
    }
}
