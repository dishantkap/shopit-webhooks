<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

use SoftDeletes;


  protected $table = 'categories';
  protected $fillable = ['name','parent_id','status','image'];

public function parents()
{
 return  $this->hasMany('App\Models\ParentCategory','id','parent_id');
}

}
