<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyTransaction extends Model
{
   protected $fillable=[
    'company_id',
    'user_id',
    'order_id',
    'card_details',
    'name_on_card',
    'auth_key',
    'message',
    'amount',
    'gateway',
    'status',
    'type',
    'created',
    'currency',
    'payment_status',
   ];

   public function companyProfile()
    {
        return $this->hasOne('App\Models\CompanyProfile', 'id', 'company_id');
    }

    public function customer()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
