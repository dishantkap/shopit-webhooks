<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopifyShippingCarrier extends Model
{
    //
    use SoftDeletes;
    protected $table = 'shopify_shipping_carriers';
    protected $fillable = ['name', 'tracking_url'];
}
