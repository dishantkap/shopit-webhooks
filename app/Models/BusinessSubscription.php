<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessSubscription extends Model
{
    use SoftDeletes;
    protected $table = 'business_subscription';
    protected $fillable = [
        'user_id', 'name', 'email', 'start_date', 'end_date', 'client_secret', 'checkout_link', 'payment_status', 'subscription_id'
    ];
}