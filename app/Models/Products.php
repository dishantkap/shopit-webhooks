<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    protected $appends = ['charge_price','status_name'];

    use SoftDeletes;

	protected $fillable = ['company_id','name','description','image','product_category_id','regular_price','sale_price','sku','is_new','is_popular','stock_status'];

	public function category(){
        return $this->hasOne('App\Models\Category', 'id', 'product_category_id');
    }

    public function getStatusNameAttribute()
    {
        return config('constant.stock_status')[$this->stock_status];
    }

    public function getChargePriceAttribute()
    {
        if(checkExists($this->sale_price)){
            return $this->sale_price;
        }else{
            return $this->regular_price;
        }
    }

    public function company()
    {
        return $this->hasOne('App\Models\CompanyProfile', 'id', 'company_id');
    }

    public function categories(){
      $cat_ids=$this->hasMany('App\Models\Category','id','product_category_id');
      return $cat_ids;
  }

  public function images()
  {
   return $this->hasMany('App\Models\ProductImage','product_id','id');
  }
  public function size()
  {
   return $this->hasMany('App\Models\ProductSize','product_id','id');
  }
  public function color()
  {
   return $this->hasMany('App\Models\SelectedProductColor','product_id','id')->with('details','images','size');
  }


}
