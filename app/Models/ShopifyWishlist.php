<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyWishlist extends Model
{
    protected $fillable=['company_id','user_id','product_id','variant_id','status'];
}
