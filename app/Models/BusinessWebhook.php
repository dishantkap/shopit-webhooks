<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessWebhook extends Model
{
    use SoftDeletes;
   protected $table="business_webhooks";
   protected $fillable=[
      'user_id',
      'webhook_type',
      'webhook_endpoint',
      'company_profile_id',
      'webhook_id'
   ];
}
