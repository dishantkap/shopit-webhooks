<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessBookingDetails extends Model
{
     protected $fillable = [
        'company_id', 'payment', 'cancellation_policy', "address_select_option", "select_service_provider", "interval_time"
    ];
}
