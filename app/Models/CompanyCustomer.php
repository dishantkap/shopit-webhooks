<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyCustomer extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'company_id', 'user_id'
    ];

    protected $talbe = 'company_customer';

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->where('type', 'C');
    }

    public function homeAddress()
    {
        return $this->hasMany('App\Models\CustomerAddress', 'customer_id', 'id')->where('type', 'H');
    }

    public function serviceAddress()
    {
        return $this->hasMany('App\Models\CustomerAddress', 'customer_id', 'id')->where('type', 'S');
    }

    public function companyDetails()
    {
        return $this->hasOne('App\Models\CompanyProfile', 'company_id','id');
    }

    public function companyAddressDetails()
    {
        return $this->hasOne('App\Models\CompanyLocation', 'company_id','id');
    }
}
