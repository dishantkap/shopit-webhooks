<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopifyVariant extends Model
{
  use SoftDeletes;
    protected $fillable=[
      'veriant_id','product_id','title','price','compare_at_price','sku','position','inventory_policy','fulfillment_service','inventory_management','option1','option2','option3','taxable','barcode','image_id','weight','weight_unit','inventory_item_id','old_inventory_quantity','inventory_quantity','require_shipping','admin_graphql_api_id','sync_at'
    ];

  public function options()
  {
   return $this->hasMany('App\Models\ShopifyOption','product_id','product_id')->with('options');
  }
}
