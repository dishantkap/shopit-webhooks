<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyLocation extends Model
{
   use SoftDeletes;
    protected $table = 'company_locations';
    protected $appends = ['rate'];

    protected $fillable = [
        'company_profile_id','type','location_nickname','country','state','city','zipcode','latitude', 'longitude'
    ];

    public $timestamps = true;

    public function company(){
    	return $this->hasOne("App\Models\CompanyProfile",'id','company_profile_id');
    }

    public function rating()
    {
      return $this->hasMany('App\Models\CompanyRating','company_id','company_profile_id');
    }

    public function getRateAttribute()
    {
      $res=$this->rating->avg('star');
      if($res==null){
        return 0;
      }else{
        return number_format((float)$res, 1, '.', '');
      }

    }
}
