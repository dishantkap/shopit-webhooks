<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyCollection extends Model
{
    protected $fillable=[
      'shopify_id',
      'company_id',
      'collection_id',
      'product_id',
      'handle',
      'title',
      'image',
      'added_date',
      'collection_type',
      'admin_graphql_api_id',
      'product_count',
      'sort_order',
      'position'
    ];
}
