<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerAddress extends Model
{
    use SoftDeletes;
    protected $table = 'customer_addresses';
    protected $fillable = [
        'customer_id','contact_name','contact_number', 'type' ,'address', 'city', 'zipcode', 'state','lat', 'lng','is_default'
    ];

    public $timestamps = true;

}
