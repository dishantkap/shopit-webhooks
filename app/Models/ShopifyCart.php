<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopifyCart extends Model
{

  use SoftDeletes;
    protected $fillable=[
      'user_id','variant_id','quantity','product_id','company_id'
    ];

  public function product()
  {
   return $this->hasMany('App\Models\ShopifyProduct','shopify_product_id','product_id');
  }

  public function variant()
  {
   return $this->hasMany('App\Models\ShopifyVariant','veriant_id','variant_id')->with('options');
  }

}
