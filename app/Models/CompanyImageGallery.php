<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyImageGallery extends Model 
{
	protected $table = "company_images_galleries";
    protected $fillable = [ 'company_id','image_url'];

}
