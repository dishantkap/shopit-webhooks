<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopifyOptionType extends Model
{
  use SoftDeletes;
    protected $fillable=[
     'option_id','product_id','title'
    ];

   
}
