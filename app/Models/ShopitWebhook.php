<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopitWebhook extends Model
{
    use SoftDeletes;
    protected $table="shopit_webhooks";
    protected $fillable=[
       'webhook_type',
       'address',
       'status'
    ];
}
