<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopifyProduct extends Model
{
  use SoftDeletes;
   protected $fillable=['shopify_product_id','company_id','title','description','vendor','product_type','publish_at','tamplate_suffix','status','published_scope','tag','admin_graph_api_id','image','has_varients'];

   public function images()
  {
   return $this->hasMany('App\Models\ShopifyImage','product_id','shopify_product_id');
  }

  public function variants()
  {
   return $this->hasMany('App\Models\ShopifyVariant','product_id','shopify_product_id');
  }

  public function options()
  {
   return $this->hasMany('App\Models\ShopifyOption','product_id','shopify_product_id')->with('options');
  }

  public function company()
  {
      return $this->hasOne('App\Models\CompanyProfile', 'id', 'company_id');
  }

}
