<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerUsedCoupon extends Model
{
    protected $table = 'customer_used_coupons';
    protected $fillable = ['customer_id', 'coupon_id'];
}
