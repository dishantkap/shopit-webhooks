<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyProfile extends Model
{
    use SoftDeletes;

    protected $table='company_profiles';
	protected $fillable = [
        'stripe_customer_id', 'name','subscription_id','currency_id','user_id','email','phone_number','description','status','subscription_status','logo','timezone_id'
    ];

    public function addressDetails(){
    	return $this->hasMany("App\Models\CompanyLocation",'company_profile_id','id');
    }

    public function currency(){
    	return $this->hasOne("App\Models\Currency",'id', 'currency_id')->where('status',1);
    }

    public function timezone(){
    	return $this->hasOne("App\Models\Timezone",'id', 'timezone_id')->where('status',1);
    }

    public function subscription(){
    	return $this->hasOne("App\Models\Subscription", 'company_id', 'id');
    }

    public function taxes(){
    	return $this->hasMany("App\Models\Tax", 'company_id', 'id');
    }

    public function workingHours()
    {
        return $this->hasOne('App\Models\BusinessHours', 'company_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\Models\Service', 'company_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->where('type', 'C');
    }

    public function business()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->where('type', 'B');
    }
    public function teamMembers()
    {
        return $this->hasMany('App\Models\TeamMember', 'company_id', 'id');
    }
    public function customers()
    {
        return $this->hasMany('App\Models\CompanyCustomer', 'company_id', 'id');
    }
    public function businessBookingDetails()
    {
       return $this->hasOne('App\Models\BusinessBookingDetails', 'company_id', 'id');
    }
    public function links()
    {
       return $this->hasOne('App\Models\AddExternalLink', 'company_id', 'id');
    }
    public function products()
    {
       return $this->hasMany('App\Models\Products', 'company_id', 'id');
    }

}