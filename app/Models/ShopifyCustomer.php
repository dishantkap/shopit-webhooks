<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyCustomer extends Model
{
  protected $fillable=[
   'user_id','customer_id','company_id'
  ];
}
