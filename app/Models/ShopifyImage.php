<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopifyImage extends Model
{
  use SoftDeletes;
    protected $fillable=[
      'product_id','image_id','position','src','alt','variant_id','width','height'
    ];
}
