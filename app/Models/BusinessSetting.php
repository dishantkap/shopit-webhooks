<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessSetting extends Model
{
  protected $fillable=[
    'company_id','user_id','tax_rate','tax_name','tax_status','onboarding_status','inclusive_tax','store_link','hmac_code	code','store_connect_status','return_days','free_delivery_upto','free_delivery_status','timestemp','access_token'
  ];
}
