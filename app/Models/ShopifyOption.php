<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopifyOption extends Model
{
  use SoftDeletes;
    protected $fillable=[
      'shopify_option_id','product_id','title','position'
    ];

    public function options()
    {
     return $this->hasMany('App\Models\ShopifyOptionType','option_id','id');
    }
}
