<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
	use SoftDeletes;

    protected $appends = ['buyer_orderid', 'payment_info'];
    protected $table = 'orders';

    protected $fillable = [
        'buyer_id', 'company_id', 'company_address_id', 'price', 'address', 'city', 'state', 'zipcode', 'email', 'contact_name', 'delivery_address',
        'status','discount', 'coupon_id', 'shipping'
    ];

	public function orderDetail()
    {
        return $this->hasMany('App\Models\OrderDetail', 'order_id', 'id')->with('product','size','color');
    }

    public function buyer()
    {
        return $this->hasOne('App\User', 'id', 'buyer_id');
    }

    public function buyer_address()
    {
        return $this->hasOne('App\Models\CustomerAddress', 'id', 'delivery_address');
    }

    public function company()
    {
        return $this->hasOne('App\Models\CompanyProfile', 'id', 'company_id');
    }

    public function companyAddress()
    {
        return $this->hasOne('App\Models\CompanyLocation', 'id', 'company_address_id');
    }


    public function getBuyerOrderidAttribute()
    {
        return $this->id;
    }

    public function getPaymentInfoAttribute()
    {
        $total = 0.00;
        foreach($this->orderDetail as $od){
            $total += $od->price * $od->quantity;
        }

        return (object)[
            'total'=>roundOff($total)
        ];
    }
}
