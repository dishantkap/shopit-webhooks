<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebhookResponse extends Model
{
    use SoftDeletes;
    protected $table="webhook_responses";
    protected $fillable=[
       'user_id','webhook_id','webhook_type','payload','status'
    ];


    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
