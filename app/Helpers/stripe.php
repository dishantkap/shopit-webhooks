<?php

use Stripe\Webhook;

function STRIPE($secret_key=''){
    $STRIPE_PRIVATE_KEY = config("constant.STRIPE_PRIVATE_KEY");
    $STRIPE_SECRET = config("constant.STRIPE_SECRET");
    $secret_key = checkExists($secret_key) ? $secret_key : $STRIPE_SECRET;
    return new \Stripe\StripeClient($secret_key);
}

function Webhook_id(){
  return config("constant.STRIPE_WEBHOOK_ID");
}


function createProduct($input)
{
    try {
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();

        $data = [
            'name' => $input['name'],
        ];

        $productData =  $stripe->products->create($data);

        // prod_L0IcZsfT4CrIfy

        if (is_object($productData) && isset($productData->id)) {
            $response->code = 200;
            $response->id = $productData->id;
            $response->result = $productData;
            $response->message="Product Created Successfully";
        }
        return $response;
    } catch (\Exception $e) {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function createPrice($input)
{
    try {
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();

        $data = [
            'product' => $input['product_id'],
            'unit_amount' => $input['amount'],
            'currency' => $input['currency'],
            'recurring' => $input['recurring'],
        ];

        $priceData =  $stripe->prices->create($data);

        if (is_object($priceData) && isset($priceData->id)) {
            $response->code = 200;
            $response->id = $priceData->id;
            $response->result = $priceData;
            $response->message = 'Price Created Successfully';
        }
        return $response;
    } catch (\Exception $e) {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function createPayment($input)
{
    try {
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();

        $data = [
            'customer' => $input['customer_id'],
            'items' => [[
                'price' => $input['price_id'],
            ]],
            'payment_behavior' => 'default_incomplete',
            // 'payment_method_types'=>[
            //     'card'
            // ],
            'expand' => ['latest_invoice.payment_intent'],
        ];

        $subscriptionData = $stripe->subscriptions->create($data);


        if (is_object($subscriptionData) && isset($subscriptionData->id)) {
            $response->code = 200;
            $response->id = $subscriptionData->id;
            $response->clientSecret = $subscriptionData->latest_invoice->payment_intent->client_secret;
            $response->result = $subscriptionData;
            $response->message = "Success";
        }
        return $response;
    } catch (\Exception $e) {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function createSubscription($input)
{
    try {
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();
        $user_id=auth()->user();
        $data = [
            'customer' => $input['customer_id'],
            'items' => [[
                'price' => $input['price_id'],
            ]],
            'expand' => ['latest_invoice.payment_intent'],
            ['metadata' => ['user_id' => $user_id]]
        ];

        $subscriptionData = $stripe->subscriptions->create($data);


        if (is_object($subscriptionData) && isset($subscriptionData->id)) {
            $response->code = 200;
            $response->message = 'Success';
            $response->id = $subscriptionData->id;
            $response->clientSecret = $subscriptionData;
            $response->result = $subscriptionData;
        }
        return $response;
    } catch (\Exception $e) {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function retriveSubscription($input)
{
    try {
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();

     $subscriptionData=   $stripe->subscriptions->retrieve(
            $input['subscription_id'],
            []
          );

        if (is_object($subscriptionData) && isset($subscriptionData->id)) {
            $response->code = 200;
            $response->id = $subscriptionData->id;
            // $response->clientSecret = $subscriptionData->latest_invoice->payment_intent->client_secret;
            $response->result = $subscriptionData;
        }
        return $response;
    } catch (\Exception $e) {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function cancelSubscription($input)
{
    try {
       
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();

        $subscriptionData=   $stripe->subscriptions->cancel(
            $input['subscription_id'],
            []
          );

        if (is_object($subscriptionData) && isset($subscriptionData->id)) {
            $response->code = 200;
            $response->id = $subscriptionData->id;
            // $response->clientSecret = $subscriptionData->latest_invoice->payment_intent->client_secret;
            $response->result = $subscriptionData;
        }
        return $response;
    } catch (\Exception $e) {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}
// CUSTOMER FUNCTION
function createStripeCustomer($input){
    // try{
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';


        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();
        $email=isset($input['email']) ? $input['email']:"";
        $phone=isset($input['phone_number']) ? $input['phone_number']:"";
        $name=isset($input['name']) ? $input['name']:"";

        $data=[
            'name'=>$name,
            'email' => $email,
            'phone' => $phone,
            'metadata' => isset($input['metadata']) ? $input['metadata'] : [],
        ];

    //    dd($data);
        $customer = $stripe->customers->create($data);



        if(is_object($customer) && isset($customer->id)){
            $response->code = 200;
            $response->id = $customer->id;
            $response->result = $customer;
        }
        return $response;
    // }catch (\Exception $e)
    // {
    //     $response->code = 500;
    //     $response->message = $e->getMessage();
    //     return $response;
    // }
}

function getStripeCustomer($input){
    $response = getObj();
    try{
        $response->code = 204;
        $response->message = 'Error';
        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();

        $customer = $stripe->customers->retrieve(
            $input['stripe_customer_id'],
            []);

        if(is_object($customer) && isset($customer->id)){
            $response->code = 200;
            $response->result = $customer;
        }
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
    }
    return $response;
}

function destroyStripeCustomer($input){
    try{
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';
        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();
        $customer = $stripe->customers->delete(
            $input['customer_id'],
            []
        );

        if(is_object($customer) && isset($customer->deleted) && $customer->deleted==true){
            $response->code = 200;
        }

        return $response;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}
// CUSTOMER FUNCITON END


// PAYMENT FUNCTION
function stripeToken($input){
    try{
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';
        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();
        $token = $stripe->tokens->create([
            'card' => [
                'number'    => str_replace(' ', '', $input['number']),
                'exp_month' => $input['exp_month'],
                'exp_year'  => $input['exp_year'],
                'cvc'       => $input['cvv'],
                'name'      => isset($input['name'])?$input['name']:""
            ]
        ]);
        if(is_object($token) && isset($token->id)){
            $response->code = 200;
            $response->id = $token->id;
            $response->result = $token;
        }
        return $response;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function stripeCharge($input){
    try{
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';
        $secret_key = isset($input['secret_key']) ? $input['secret_key'] : '';
        $stripe = STRIPE($secret_key);
        $amount = roundOff($input['amount']);
        $data = [
            'amount' => (float)$amount,
            'currency' => $input['currency']
        ];
        if(isset($input['stripe_token'])){
            $data['source'] = $input['stripe_token'];
        }
        if(isset($input['customer_id'])){
            $data['customer'] = $input['customer_id'];
        }
        $charge = $stripe->charges->create($data);
        if(is_object($charge) && isset($charge->id)){
            $response->code = 200;
            $response->id = $charge->id;
            $response->result = $charge;
        }
        return  $response;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function stripeRefund($input){
    try{
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';
        $stripe = STRIPE($input['secret_key']);

        $amount = roundOff($input['amount']*100);
      //  $amount= intval($amount);
        $refund = $stripe->refunds->create([
            'amount' => (float)$amount,
            'charge' => $input['charge_id'],
        ]);

        if(is_object($refund) && isset($refund->id)){
            $response->code = 200;
            $response->message = 'success';
            $response->id = $refund->id;
            $response->result = $refund;
        }
        return  $response;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function getStripeCharge($input){
    try{
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';

        $stripe = STRIPE($input['secret_key']);

        $charge = $stripe->charges->retrieve(
            $input['stripe_charge_id'],
            []
        );

        if(is_object($charge) && checkExists($charge)){
            $response->code = 200;
            $response->message = 'success';
            $response->result = $charge;
        }
        return $response;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}
//END PAYMENT FUNCTION


// CARD FUNCITON
function addStripeCard($input){
    try{
        $response = getObj();
        $response->code = 204;
        $response->message = 'Error';
        $token = stripeToken($input);

        // number , exp_month , exp_year , cvv, stripe_customer_id
        if($token->code ==200){

            $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();
            $card = $stripe->customers->createSource(
                $input['stripe_customer_id'],[
                'source'=> $token->id
            ]);
            if(is_object($card) && isset($card->id)){
                $response->code = 200;
                $response->id = $card->id;
                $response->result = $card;
                $response->message = "Card Added Successfully";

            }else{
                $response->message = $card->message;
            }
        }else{
            $response->message = $token->message;
        }
        return $response;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function removeStripeCard($input){
    $response = getObj();
    try{
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();
        $card = $stripe->customers->deleteSource(
            $input['stripe_customer_id'],
            $input['stripe_card_id'],
            []
        );
        $response->message="Success";
        $response->code = 200;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
    }
    return $response;
}

function makeStripeCardDefault($input){
    $response = getObj();
    try{
        $response->code = 204;
        $response->message = 'Error';

        $customer = getStripeCustomer([
            'stripe_customer_id'=>$input['stripe_customer_id']
        ]);
        if($customer->code == 200){
            $customer = $customer->result;
            $customer->default_source = $input['stripe_card_id'];
            $customer->save();
            $response->message = 'Card Set Default Successfully';
            $response->code = 200;
        }else{
            $response->message = $customer->message;
        }
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
    }
    return $response;
}

function getStripeCard($input){
    $response = getObj();
    try{
        $response->code = 204;
        $response->message = 'Error';

        $stripe = isset($input['secret_key']) && checkExists($input['secret_key']) ? STRIPE($input['secret_key']) : STRIPE();

        $cards = $stripe->customers->allSources(
            $input['stripe_customer_id']
        );

        if(is_object($cards) && checkExists($cards)){
            $response->code = 200;
            $response->message = 'Success';
            $response->result = $cards;
        }
        return $response;
    }catch (\Exception $e)
    {
        $response->code = 500;
        $response->message = $e->getMessage();
    }
    return $response;
}


function payment_status(){
  $endpoint_secret = Webhook_id();

$payload = @file_get_contents('php://input');
$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
$event = null;

try {
    $event = Webhook::constructEvent(
        $payload, $sig_header, $endpoint_secret
    );
} catch(\UnexpectedValueException $e) {
    // Invalid payload
    http_response_code(400);
    exit();
} catch(\Stripe\Exception\SignatureVerificationException $e) {
    // Invalid signature
    http_response_code(400);
    exit();
}

if ($event->type == "payment_intent.succeeded") {
    $intent = $event->data->object;
    printf("Succeeded: %s", $intent->id);
    http_response_code(200);
    exit();
} elseif ($event->type == "payment_intent.payment_failed") {
    $intent = $event->data->object;
    $error_message = $intent->last_payment_error ? $intent->last_payment_error->message : "";
    printf("Failed: %s, %s", $intent->id, $error_message);
    http_response_code(200);
    exit();
}
}

// Setting Web Hook Link

function setWebhook($input)
{
    $stripe = STRIPE();
    $response=[];
   $webhook_response=   $stripe->webhookEndpoints->create([
        'url' => $input['url'],
        'enabled_events' => [
          'charge.failed',
          'charge.succeeded',
        ]
      ]);
      $response['code'] = 200;
      $response['message']="Webhook Added Successfully";
      $response['result']=$webhook_response;

      return $response;
}

// Create Account for Business
function createAccount($input){
  $response = getObj();
    $stripe = STRIPE();
   $acc= $stripe->accounts->create([
        'type' => 'express',
        'country' => 'US',
        'email' => $input['email'],
        'capabilities' => [
          'card_payments' => ['requested' => true],
          'transfers' => ['requested' => true],
        ],
        'metadata'=>["business_profile"=>$input['business_profile_id'],'user_id'=>$input['user_id']],
      ]);
      $response->code=200;
      $response->result=$acc;
      return $response;
}


function account_transfer($input){
  try{


  $response=getObj();
  $stripe = STRIPE();
  $amount=roundOff($input['amount']);
  // $amt=floatval($amount);
  $status=$stripe->transfers->create([
    'amount' => $amount*100,
    'currency' => 'usd',
    'destination' => $input['payment_id'],
    'transfer_group' => $input['order_id'],
  ]);

  $response->code=200;
  $response->result=$status;

  return $response;

}catch (\Exception $e)
{
  $response=getObj();
    $response->code = 500;
    $response->message = $e->getMessage();

  return $response;
}
}


function onboarding($input){
  $response=getObj();
  $stripe = STRIPE();
 $resp= $stripe->accountLinks->create([
    'account' => $input['account_id'],
    'refresh_url' => $input['refresh_url'],
    'return_url' =>  $input['return_url'],
    'type' => 'account_onboarding',
  ]);
  $response->message="link Generated Successfully";
  $response->code=200;
  $response->result=$resp;

  return $response;
}

function account_status($input){
  $stripe = STRIPE();
  $response=getObj();
  $status=$stripe->accounts->retrieve(
     $input['account_id'],
    []
  );

  $response->code=200;
  $response->message="Account Retrived Successfully";
  $response->result=$status;

  return $response;
}




function reject_account($input)
{
  $response=getObj();
  $stripe = STRIPE();
  $respo=$stripe->accounts->reject(
    $input['account_id'],
    ['reason' => $input['reason']]
  );


  $response->code=200;
  $response->result=$respo;

  return $response;

}


function delete_account($input){
  $response=getObj();
  $stripe = STRIPE();
  $out=$stripe->accounts->delete(
    $input['account_id'],
    []
  );


  $response->code=200;
  $response->result=$out;

 return $response;

}