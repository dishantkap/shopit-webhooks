<?php

function STRIPE(){
    return new \Stripe\StripeClient(config('constant.STRIPE_SECRET'));
}

function subscribe($input){
    try{
        $response = (object)[];
        $response->code = 204;
        $response->message = 'Error';
        $stripe = STRIPE();

        $subscribeData= $stripe->subscriptions->create([            
            'customer' => $input['stripe_customer_id'],
            "default_payment_method"=>$input['default_payment_method'],
            'items' => [
                ['price' => $input["price"]],
            ]]
          );
  
        if(is_object($subscribeData) && !empty($subscribeData->id)){
            $response->code = 200;           
            $response->message = "You have subscribed successfully!";
            $response->id = $subscribeData->id;         
        }else{
            $response->code = 204;           
            $response->message = "You are unable to subscribe,Try again later!";
            $response->result = [];
        }
        return $response;
    }catch(\Exception $e){
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }

}

function cardList($input){
    try{
        $response = (object)[];
        $response->code = 204;
        $response->message = 'Error';
        $stripe = STRIPE();

        $cardData= $stripe->customers->allSources(
            $input['stripe_customer_id'],
            ['object' => 'card', 'limit' => 10]
          );
        if(is_array($cardData['data']) && count($cardData['data'])>0){
            $response->code = 200;           
            $response->message = "Card list found successfully!";
            $response->result = $cardData['data'];
        }else{
            $response->code = 204;           
            $response->message = "No Card Found";
            $response->result = [];
        }
        return $response;
    }catch(\Exception $e){
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function addcard($input){
    try{
        $response = (object)[];
        $response->code = 204;
        $response->message = 'Error';
        $stripe = STRIPE();

        $cardData=  $stripe->customers->createSource(
            $input['stripe_customer_id'],
            ['source' =>  $input['token']]
          );

        if(is_object($cardData) && isset($cardData->id)){
            $response->code = 200;
            $response->id = $cardData->id;
            $response->message = "Card added successfully!";
            $response->result = $cardData;
        }
        return $response;

    }catch(\Exception $e){
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
   
  
}

function createCustomer($input){
    try{
        $response = (object)[];
        $response->code = 204;
        $response->message = 'Error';
        $stripe = STRIPE();
        $customer = $stripe->customers->create([
            'email' => $input['email'],
            'phone' => isset($input['phone_number'])?$input['phone_number']:'',
            'metadata' => isset($input['metadata']) ? $input['metadata'] : [],
        ]);
        if(is_object($customer) && isset($customer->id)){
            $response->code = 200;
            $response->id = $customer->id;
            $response->result = $customer;
        }
        return $response;
    }catch (\Exception $e) 
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function stripeToken($input){
    try{
        $response = (object)[];
        $response->code = 204;
        $response->message = 'Error';
        $stripe = STRIPE();
        $token = $stripe->tokens->create([
            'card' => [
                'number'    => $input['number'],
                'exp_month' => $input['exp_month'],
                'exp_year'  => $input['exp_year'],
                'cvc'       => $input['cvv'],
            ]
        ]);
        if(is_object($token) && isset($token->id)){
            $response->code = 200;
            $response->id = $token->id;
            $response->result = $token;
        }
        return $response;
    }catch (\Exception $e) 
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}

function stripeCharge($input){
    try{
        $response = (object)[];
        $response->code = 204;
        $response->message = 'Error';
        $stripe = STRIPE();
        $amount = $input['amount'] * 100;
        $data =[
            'amount' => $amount ,
            'currency' => $input['currency']
        ];
        if(isset($input['stripe_token_id'])){
            $data['source'] = $input['stripe_token_id'];
        }
        $charge = $stripe->charges->create($data);
        if(is_object($charge) && isset($charge->id)){
            $response->code = 200;
            $response->id = $charge->id;
            $response->result = $charge;
        }
        return  $response;
    }catch (\Exception $e) 
    {
        $response->code = 500;
        $response->message = $e->getMessage();
        return $response;
    }
}