<?php

use App\Models\BusinessSetting;
use App\Models\Checkout;
use App\Models\ShopifyCart;
use App\Models\ShopifyCustomer;
use App\Models\User;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Shopify\Clients\Rest;
use Shopify\Clients\Http;



function token_generate()
{

  $user_id = auth()->user()->id;
  $details = BusinessSetting::where('user_id', $user_id)->first();
  $response = getObj();

  $url = 'https://' . $details->store_link . '/admin/oauth/access_token';

  $client = new \GuzzleHttp\Client();
  try {
    $responsez = $client->request('POST', $url, [
      'form_params' => [
        'client_id' => config('constant.SHOPIFY_API_KEY'),
        'client_secret' => config('constant.SHOPIFY_SECRET_KEY'),
        'code' => $details->code,
        // 'code'=>'8ac46185ff6a803eb587658ad35adb58'
      ]
    ]);
    $token_data = $responsez->getBody()->getContents();
    $token_data = json_decode($token_data);
    $response->code = 200;
    $response->result = $token_data->access_token;

    $update = BusinessSetting::where('user_id', $user_id)->update(['access_token' => $token_data->access_token]);
  } catch (BadResponseException $e) {
    $response->code = 400;
  }

  return redirect('settings/payment');
}

function get_access($company_id)
{
  $business = BusinessSetting::where('company_id', $company_id)->first();
  $arr = ['url' => $business->store_link, 'access_token' => $business->access_token,'hmac'=> $business->hmac_code];

  return $arr;
}

function shopify_access_token($input){
        $shop = "shine-web-solutions";
        $api_key =config('constant.SHOPIFY_API_KEY');
        $scopes = "read_orders,write_products,read_products,write_customers,read_price_rules,write_checkouts,write_orders,read_shipping";
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://"; 
        $base_url= $protocol. $_SERVER['SERVER_NAME'];
        $redirect_uri = $base_url."/generate_token";
        // dd($redirect_uri);
        // $redirect_uri = "http://127.0.0.1:8000/generate_token";

        // Build install/approval URL to redirect to
        $install_url = "https://".$input['shop']."/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

  // Build install/approval URL to redirect to
  $install_url = "https://" . $input['shop'] . "/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

  return $install_url;
}

function products($shop_domain)
{
  // require env('APP_URL').'app/SHOPIFY/vendor/autoload.php';
  $requestUrl = 'https://' . config('constant.SHOPIFY_API_KEY') . ':' . config('constant.SHOPIFY_SECRET_KEY') . '@' . $shop_domain['url'] . '/admin/products.json';

  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $shop_domain['access_token']
    ]
  ]);

  $products = json_decode($response->getBody());
  return $products;
}

function single_product($input)
{
  $requestUrl = 'https://' . $input['url'] . '/admin/api/2021-10/products/' . $input['product_id'] . '.json';

  $client = new \GuzzleHttp\Client();
  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $input['access_token']
    ]
  ]);

  $product = json_decode($response->getBody());
  return $product;
}


function shop($shop_domain)
{
  $requestUrl = 'https://' . $shop_domain['url'] . '/admin/api/2021-07/shop.json';

  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $shop_domain['access_token']
    ]
  ]);

  $shop = json_decode($response->getBody());
  return $shop;
}

function create_shopify_user($details)
{

  try {

    $user_id = $details['user_id'];

    $user_details = User::where('id', $user_id)->where('type', 'C')->first();

    if ($user_details == null) {
      $error = getObj();
      $error->message = "Invalid User";
      $error->code = "401";
      dd($error);
    }
    $company = get_access($details['company_id']);
    if ($user_details->last_name != null) {
      $info = [
        "customer" =>
        [
          "first_name" => $user_details->first_name,
          "last_name" => $user_details->last_name,
          "email" => $user_details->email,
          "phone" => $user_details->phone_number,
          "verified_email" => false,
        ]
      ];
    } else {
      $info = [
        "customer" =>
        [
          "first_name" => $user_details->first_name,
          "last_name" => " ",
          "email" => $user_details->email,
          "phone" => $user_details->phone_number,
          "verified_email" => true,
        ]
      ];
    }


  
    // $info_obj=json_encode($info);
    $requestUrl = 'https://' . $company['url'] .'/admin/api/2021-10/customers.json';
    $client = new \GuzzleHttp\Client();

    $response = $client->request('POST', $requestUrl, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Shopify-Access-Token' => $company['access_token']
      ],
      'json' => $info
    ]);

    $user_response = json_decode($response->getBody());

   
    return $user_response;
  } catch (ClientException $ce) {
    if ($ce->hasResponse()) {
      $response = [];
      $message = $ce->getResponse();
      $res = json_decode($message->getBody());
      // dd($res);
      $user_id = $details['user_id'];

      if(isset($res->errors->phone)){
        $rep_arr=['code'=>202,'message'=>$res->errors->phone[0],'result'=>[]];
        if($res->errors->phone[0]!='Phone has already been taken'){
          print_r(json_encode($rep_arr,JSON_PRETTY_PRINT));
          exit();
          die;
        }
      }
      if(isset($res->errors->email)){
        $response['message'] = $res->errors->email[0];
        $company = get_access($details['company_id']);
        $user_details = User::where('id', $user_id)->where('type', 'C')->first();
        // dd($user_details);
        $fetch = list_shopify_users(['url' => $company['url'], 'access_token' => $company['access_token']]);
       
        foreach ($fetch->customers as $info) {
          if (strtolower($info->email) == strtolower($user_details->email) || $info->phone == $user_details->phone_number) {
            return $info;
          }
        }
      }else{
        $rep_arr=['code'=>202,'message'=>'Error ','result'=>$res];
        print_r(json_encode($rep_arr,JSON_PRETTY_PRINT));
        exit();
        die;
      }
      return $response;
    } else {
      $message = "No response";
    }
  } catch (RequestException $re) {
    if ($re->hasResponse()) {
      $message = $re->getResponse();
    } else {
      $message = "No response";
    }
  } catch (Exception $e) {

    $message = $e->getMessage();
  }
}


function list_shopify_users($input)
{

    $requestUrl = 'https://'.$input['url']. '/admin/api/2021-10/customers.json';


  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $input['access_token']
    ]
  ]);

  $shop = json_decode($response->getBody());
  return $shop;
}

function list_shopify_user($input)
{

    $requestUrl = 'https://'.$input['url']. '/admin/api/2021-10/customers.json?ids='.$input['ids'];


  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $input['access_token']
    ]
  ]);

  $shop = json_decode($response->getBody());
  return $shop;
}

function add_address($input)
{
  $address = [
    "address" => [
      "address1" => $input['address1'],
      "address2" => $input['address2'],
      "city" => $input['city'],
      "first_name" => $input['first_name'],
      "last_name" => $input['last_name'],
      "phone" => $input['phone'],
      "province" => $input['state'],
      "country" => $input['country'],
      "zip" => $input['postal_code'],
      "name" => $input['first_name'] . " " . $input['last_name'],
      "province_code" => $input['provience_code'],
      "country_code" => $input['country_code'],
      "country_name" => $input['country'],
      "default" => $input['default']
    ]
  ];

  $user_id = $input['user_id'];
  $usr = ShopifyCustomer::where(['user_id' => $user_id, 'company_id' => $input['company_id']])->first();

  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/customers/' . $usr->customer_id . '/addresses.json';
  $client = new \GuzzleHttp\Client();



  $response = $client->request('POST', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $address
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function update_address($input)
{

  $user = ShopifyCustomer::where(['user_id' => $input['user_id'], "company_id" => $input['company_id']])->first();
  $address = [
    "address" => [
      "id" => $input['address_id'],
      "address1" => $input['address1'],
      "address2" => $input['address2'],
      "city" => $input['city'],
      "first_name" => $input['first_name'],
      "last_name" => $input['last_name'],
      "phone" => $input['phone'],
      "province" => $input['state'],
      "country" => $input['country'],
      "zip" => $input['postal_code'],
      "name" => $input['first_name'] . " " . $input['last_name'],
      "province_code" => $input['provience_code'],
      "country_code" => $input['country_code'],
      "country_name" => $input['country'],
      "default" => $input['default']
    ]
  ];

  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/customers/' . $user->customer_id . '/addresses/' . $input['address_id'] . '.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('PUT', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $address
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}
function remove_address($input)
{

  $user = ShopifyCustomer::where(['user_id' => $input['user_id'], "company_id" => $input['company_id']])->first();
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/customers/' . $user->customer_id . '/addresses/' . $input['address_id'] . '.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('DELETE', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function list_address($input)
{

  $user = ShopifyCustomer::where(['user_id' => $input['user_id'], "company_id" => $input['company_id']])->first();
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/customers/' . $user->customer_id . '/addresses.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function make_address_default($input)
{

  $user = ShopifyCustomer::where(['user_id' => $input['user_id'], "company_id" => $input['company_id']])->first();
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/customers/' . $user->customer_id . '/addresses/' . $input['address_id'] . '/default.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('PUT', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function varient_product($input)
{


  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/products/' . $input['product_id'] . '/variants.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function price_rules($input)
{


  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/price_rules.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function apply_coupon($input)
{


  $business = get_access($input['company_id']);
  $checkout_token=$input['checkout_token'];
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/checkouts/'.$checkout_token.'.json';

  $data=[
    "checkout"=>
    [
    "token"=>$checkout_token,
    "discount_code"=>$input['coupon']
    ]
  ];

  $client = new \GuzzleHttp\Client();

  $response = $client->request('PUT', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json'=>$data
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function collections_list($input)
{

  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/collects.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());

  // dd($user_response);

  foreach ($user_response->collects as $resp) {
    $requestUrl1 = 'https://' . $business['url'] . '/admin/api/2021-10/collections/' . $resp->collection_id . '.json';
    $client = new \GuzzleHttp\Client();

    $responsez = $client->request('GET', $requestUrl1, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Shopify-Access-Token' => $business['access_token']
      ],
    ]);

    $resp->details = json_decode($responsez->getBody());
  }

  return $user_response;
}


function collection_products($input)
{

  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/collections/' . $input['collection_id'] . '/products.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function create_checkout($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/checkouts.json';
  $client = new \GuzzleHttp\Client();
  $info = [
    "checkout" => [
      "line_items" => [],
      "discount_code"=>(isset($input['discount_code']))?$input['discount_code']:''
    ]
  ];

  $user_id = $input['user_id'];
  $list_items = ShopifyCart::where(['user_id' => $user_id, 'company_id' => $input['company_id']])->get();

  foreach ($list_items as $item) {
    array_push($info['checkout']['line_items'], ["variant_id" => $item->variant_id, "quantity" => $item->quantity]);
  }

  $response = $client->request('POST', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $info
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function admin_list_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json?status=' . $input['status'] . '&ids=' . $input['order_ids'];
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function list_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function list_cancel_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json?status=cancelled';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function list_fulfill_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json?status=closed';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function checkout_status($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/checkouts/' . $input['token'] . '.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function add_card($input)
{
  $business = get_access($input['company_id']);
  $details = Checkout::where('checkout_token', $input['checkout_token'])->first();
  $requestUrl = $details->api_payment_url;
  $client = new \GuzzleHttp\Client();

  $data = [
    "credit_card" => [
      "number" => $input['card_number'],
      "first_name" => $input['first_name'],
      "last_name" => $input['last_name'],
      "month" => $input['month'],
      "year" => $input['year'],
      "verification_value" => $input['cvv']
    ]
  ];
  $response = $client->request('POST', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $data
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}


function single_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders/' . $input['order_id'] . '.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function cancel_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders/' . $input['order_id'] . '/cancel.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('POST', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => []
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function refund_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders/' . $input['order_id'] . '/refunds.json';
  $client = new \GuzzleHttp\Client();
  $response = $client->request('POST', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $input['data'],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function fulfill_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders/' . $input['order_id'] . '/fulfillments.json';
  $client = new \GuzzleHttp\Client();
  $response = $client->request('POST', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $input['data'],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function delete_orders($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders/' . $input['order_id'] . 'json';
  $client = new \GuzzleHttp\Client();
  $response = $client->request('DELETE', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => [],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function product_create_webhook($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/webhooks.json';
  $client = new \GuzzleHttp\Client();
  // type=> products/create
  $data = [
    "webhook" => [
      "topic" => $input['type'],
      "address" => "https://admin.shopitnow.app/api/v1/shopify/webhook/".$input['type']."/" . encode($input['company_id']),
      "format" => "json",
    ]
  ];


  $response = $client->request('POST', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $data
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function update_checkout($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/checkouts/' . $input['checkout_token'] . '.json';
  $client = new \GuzzleHttp\Client();
  $data = [
    "checkout" => [
      "token" => $input['checkout_token'],
      'email' => $input['email'],
      "shipping_address" => [
        'first_name' => $input['first_name'],
        'last_name' => $input['last_name'],
        'address1' => $input['address1'],
        'address2' => $input['address2'],
        'city' => $input['city'],
        'province_code' => $input['province_code'],
        'phone' => $input['phone'],
        'country_code' => $input['country_code'],
        'zip' => $input['zip'],
      ]
    ]
  ];


  $response = $client->request('PUT', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    'json' => $data
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function customer_get($input)
{
  $business = get_access($input['company_id']);
  $user = ShopifyCustomer::where(['user_id' => $input['user_id'], 'company_id' => $input['company_id']])->first();
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/customers/' . $user->customer_id . '.json';
  $client = new \GuzzleHttp\Client();
  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
    // 'json'=>$input['data'],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function single_user_completed_order($input)
{

  $business = get_access($input['company_id']);

  $user_id = $input['user_id'];

  $orders = Checkout::where(['business_id' => $input['company_id'], 'user_id' => $user_id, 'payment_status' => 1])->get();

  $id = "";
  foreach ($orders as $ids) {
    $id = $id . "," . $ids->order_number;
  }

  $list_ids = trim($id, ',');

  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json?ids=' . $list_ids . "&status=closed";
  // dd($requestUrl);
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function single_user_cancelled_order($input)
{

  $business = get_access($input['company_id']);

  $user_id = $input['user_id'];

  $orders = Checkout::where(['business_id' => $input['company_id'], 'user_id' => $user_id, 'payment_status' => 1])->get();
  // $orders = Checkout::where(['business_id' => $input['company_id'], 'user_id' => $user_id, 'payment_status' => 2,'order_status'=>'Cancelled'])->get();

  $id = "";
  foreach ($orders as $ids) {
    $id = $id . "," . $ids->order_number;
  }

  $list_ids = trim($id, ',');

  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json?ids=' . $list_ids . "&status=cancelled";
  // dd($requestUrl);
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function user_cancelled_order($input)
{

  $business = get_access($input['company_id']);

  $user_id = $input['user_id'];

  // $orders = Checkout::where(['business_id' => $input['company_id'], 'user_id' => $user_id, 'payment_status' => 1])->get();
  $orders = Checkout::where(['business_id' => $input['company_id'], 'user_id' => $user_id, 'payment_status' => 2,'order_status'=>'Cancelled'])->get();

  $id = "";
  foreach ($orders as $ids) {
    $id = $id . "," . $ids->order_number;
  }

  $list_ids = trim($id, ',');

  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json?ids=' . $list_ids . "&status=cancelled";
  // dd($requestUrl);
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}


function single_user_pending_order($input)
{

  $business = get_access($input['company_id']);

  $user_id = $input['user_id'];

  $orders = Checkout::where(['business_id' => $input['company_id'], 'user_id' => $user_id, 'payment_status' => 1])->get();

  $id = "";
  foreach ($orders as $ids) {
    $id = $id . "," . $ids->order_number;
  }

  $list_ids = trim($id, ',');

  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders.json?ids=' . $list_ids . "&status=open";
  // dd($requestUrl);
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function get_single_transaction($input)
{
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/orders/' . $input['order_id'] . '/transactions.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('GET', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);
  $user_response = json_decode($response->getBody());
  return $user_response;
}

function delete_customer($input)
{

  // $user=ShopifyCustomer::where(['user_id'=>$input['user_id'],"company_id"=>$input['company_id']])->first();
  $business = get_access($input['company_id']);
  $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/customers/' . $input['customer_id'] . '.json';
  $client = new \GuzzleHttp\Client();

  $response = $client->request('DELETE', $requestUrl, [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'X-Shopify-Access-Token' => $business['access_token']
    ],
  ]);

  $user_response = json_decode($response->getBody());
  return $user_response;
}

function create_hook($input)
{
  $url = $input['url'];
  $company_id = $input['company_id'];
  $topic = $input['topic'];

  $data = [
    "webhook" => [
      "topic" => $topic,
      "address" => $url,
      "format" => "json"
    ]
  ];
  
  $business = get_access($company_id);
  try {
    $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/webhooks.json';

    $ch = curl_init();
    $curlConfig = array(
        CURLOPT_URL            => $requestUrl,
        CURLOPT_POST           => true,
        CURLOPT_HTTPHEADER => ['Content-Type: application/json',
                                'X-Shopify-Access-Token: '.$business['access_token'],
                                'X-Shopify-Hmac-Sha256: '.$business['hmac'],
                                "Accept:application/json"
                              ],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS     => json_encode($data)
    );
    curl_setopt_array($ch, $curlConfig);
    $result = curl_exec($ch);
    curl_close($ch);
    $user_response = json_decode($result,true);
    // dd($user_response);
    // $client = new \GuzzleHttp\Client();
    // $response = $client->request('POST', $requestUrl, [
    //   'headers' => [
    //     'Content-Type' => 'application/json',
    //     'Accept' => 'application/json',
    //     'X-Shopify-Access-Token' => $business['access_token']
    //   ],
    //   'json' => $data,
    // ]);
    // $user_response = json_decode($response->getBody());

    return $user_response;
  } catch (ClientException $ce) {
    if ($ce->hasResponse()) {
      $message = $ce->getResponse();
    } else {
      $message = "No response";
    }
  } catch (RequestException $re) {
    if ($re->hasResponse()) {
      $message = $re->getResponse();
    } else {
      $message = "No response";
    }
  } catch (Exception $e) {

    $message = $e->getMessage();
  }
}

// function update_hook($input)
// {
//   $url = $input['url'];
//   $webhook_id = $input['webhook_id'];

//   $data = [
//     "webhook" => [
//       "id" => $webhook_id,
//       "address" => $url,
//     ]
//   ];
  
//   $business = get_access($company_id);

//   try {
//     $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/webhooks/'.$webhook_id.'.json';

//     $ch = curl_init();
//     $curlConfig = array(
//         CURLOPT_URL            => $requestUrl,
//         CURLOPT_CUSTOMREQUEST  => "PUT",
//         CURLOPT_HTTPHEADER => ['Content-Type: application/json',
//                                 'X-Shopify-Access-Token: '.$business['access_token'],
//                                 "Accept:application/json"
//                               ],
//         CURLOPT_RETURNTRANSFER => true,
//         CURLOPT_POSTFIELDS     => json_encode($data)
//     );
//     curl_setopt_array($ch, $curlConfig);
//     $result = curl_exec($ch);
//     curl_close($ch);
//     $user_response = json_decode($result,true);

//     return $user_response;
//   } catch (ClientException $ce) {
//     if ($ce->hasResponse()) {
//       $message = $ce->getResponse();
//     } else {
//       $message = "No response";
//     }
//   } catch (RequestException $re) {
//     if ($re->hasResponse()) {
//       $message = $re->getResponse();
//     } else {
//       $message = "No response";
//     }
//   } catch (Exception $e) {

//     $message = $e->getMessage();
//   }
// }

function list_webhooks($input)
{
  $company_id = $input['company_id'];

  $business = get_access($company_id);
  try {
    $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/webhooks.json';
    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', $requestUrl, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Shopify-Access-Token' => $business['access_token']
      ]
    ]);
    $user_response = json_decode($response->getBody());
    return $user_response;
  } catch (ClientException $ce) {
    if ($ce->hasResponse()) {
      $message = $ce->getResponse();
    } else {
      $message = "No response";
    }
  } catch (RequestException $re) {
    if ($re->hasResponse()) {
      $message = $re->getResponse();
    } else {
      $message = "No response";
    }
  } catch (Exception $e) {

    $message = $e->getMessage();
  }
}


function remove_webhooks($input)
{
  $company_id = $input['company_id'];

  $business = get_access($company_id);
  try {
    $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/webhooks/'.$input['webhook_id'].'.json';
    $client = new \GuzzleHttp\Client();
    $response = $client->request('DELETE', $requestUrl, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Shopify-Access-Token' => $business['access_token']
      ]
    ]);
    $user_response = json_decode($response->getBody());
    return $user_response;
  } catch (ClientException $ce) {
    if ($ce->hasResponse()) {
      $message = $ce->getResponse();
    } else {
      $message = "No response";
    }
  } catch (RequestException $re) {
    if ($re->hasResponse()) {
      $message = $re->getResponse();
    } else {
      $message = "No response";
    }
  } catch (Exception $e) {

    $message = $re->getMessage();
  }
}


function update_product($input)
{
  $company_id = $input['company_id'];

  $business = get_access($company_id);
  try {
    $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/webhooks/'.$input['webhook_id'].'.json';
    $client = new \GuzzleHttp\Client();
    $response = $client->request('DELETE', $requestUrl, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Shopify-Access-Token' => $business['access_token']
      ]
    ]);
    $user_response = json_decode($response->getBody());
    return $user_response;
  } catch (ClientException $ce) {
    if ($ce->hasResponse()) {
      $message = $ce->getResponse();
    } else {
      $message = "No response";
    }
  } catch (RequestException $re) {
    if ($re->hasResponse()) {
      $message = $re->getResponse();
    } else {
      $message = "No response";
    }
  } catch (Exception $e) {

    $message = $re->getMessage();
  }
}



function cancelation_request($input)
{
  $company_id = $input['company_id'];

  $business = get_access($company_id);

  $data=[
    "cancellation_request"=>[
      "message"=>$input['message']
      ]
    ];
  
  try {
    $requestUrl = 'https://' . $business['url'] . '/admin/api/2021-10/fulfillment_orders/'.$input['order_id'].'/cancellation_request.json';
    $client = new \GuzzleHttp\Client();
    $response = $client->request('POST', $requestUrl, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'X-Shopify-Access-Token' => $business['access_token']
      ],
      'json' => $data
    ]
    );
    $user_response = json_decode($response->getBody());
    return $user_response;
  } catch (ClientException $ce) {
    if ($ce->hasResponse()) {
      $message = $ce->getResponse();
    } else {
      $message = "No response";
    }
  } catch (RequestException $re) {
    if ($re->hasResponse()) {
      $message = $re->getResponse();
    } else {
      $message = "No response";
    }
  } catch (Exception $e) {

    $message = $e->getMessage();
  }
}









