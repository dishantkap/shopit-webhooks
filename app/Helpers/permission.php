<?php
function getRoles(){
    $roles = \App\Models\Role::all();
    return $roles;
}

function getActions(){
    $actions = \App\Models\Action::all();
    return $actions;
}
function roleAction($role_id,  $action_id){
    $roleAction = \App\Models\RoleAction::where(['role_id'=>$role_id, 'action_id'=> $action_id])->first();
    if($roleAction){
        return makeIdsEncode($roleAction);
    }
    return null;
}

function checkPermission($role, $action, $company_id, $user_id=0, $checkOnlyHeader=false){
    $user_id = $user_id != 0 ? $user_id : auth()->id();
    $user = App\User::where('id', $user_id)->first();
    if($user){
        if(($user->type == 'B' || $user->type=='A') && $user->companyId() == $company_id){
            return true;
        }
        if($user->type == 'T'){
            $team_member = App\Models\TeamMember::where(['company_id'=>$company_id, 'user_id'=>$user_id])->first();
            if($team_member){
                $role_data = App\Models\Role::where('sub_name',$role)->first();
                if($checkOnlyHeader && $role_data){
                    $role_action_id = App\Models\RoleAction::where(['role_id'=>$role_data->id])->pluck('id')->toArray();
                    $response = App\Models\TeamMemberRoleAction::where('team_member_id',$team_member->id)->whereIn('role_action_id', $role_action_id)->first();
                    if($response){
                        return true;
                    }
                }else{
                    $action_data = App\Models\Action::where(['sub_name'=>$action])->first();
                    if($action_data && $role_data){
                        $role_action = App\Models\RoleAction::where(['role_id'=>$role_data->id, 'action_id'=>$action_data->id])->first();
                        if($team_member && $role_action){
                            $response = App\Models\TeamMemberRoleAction::where(['team_member_id'=>$team_member->id, 'role_action_id'=>$role_action->id])->first();
                            if($response){
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }
    return false;
}

function makePermissionString($permission_array=array()){
    $string = '';
    if(checkExists($permission_array)){
        $allPermissionDetails  = allPermissionDetails();
        $final_array = array();
        for($i=0; $i < sizeof($permission_array);$i++){
            $detail = explode('-', $permission_array[$i]);
            $section = $detail[0];
            $option_value = $detail[1];
            if(checkExists($allPermissionDetails[$section])){
                $perticular_perm = $allPermissionDetails[$section];
                if(in_array($option_value, $perticular_perm['options'])){
                    if(array_key_exists($section, $final_array)){
                        $options = $final_array[$section]['options'];
                        array_push($options, $option_value);
                        $final_array[$section]['options'] = $options;
                    } else {
                        $final_array[$section] = $allPermissionDetails[$section];
                        $final_array[$section]['options'] = array($option_value);
                    }
                }
            }
            $string = json_encode($final_array);
        }
    }
    return $string;
}

function permissionDesign($details='', $disable_checkbox = 0){
    $details = json_decode($details);
    $permission_details = allPermissionDetails();
    $html = '';
    $html .='<tbody>';

    foreach ($permission_details as $header_name=>$permission) {
            $html .='<tr>';
            $html .='<td>
                         <a href="'.$permission['page_link'].'" target="_blank">
                            '.$permission['name'].'
                         </a>
                    </td>';
            $option = $permission['options'];
            for($i=0; $i<sizeof($option) ;$i++) {
                $current_pem_array = array();
                if(checkExists($details)){
                    if(isset($details->$header_name)){
                        if(isset($details->$header_name->options)){
                            $current_pem_array = $details->$header_name->options;
                        }
                    }
                }
                $html .='<td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="'.$header_name.'-'.$option[$i].'" class="custom-control-input" type="checkbox" name="'.$header_name.'-'.$option[$i].'"';
                                $html .= in_array($option[$i], $current_pem_array) ? "checked" : "" ;
                                $html .= $disable_checkbox == 1 ? " disabled" : "" ;
                                $html .= '><label class="custom-control-label" for="'.$header_name.'-'.$option[$i].'"></label>
                            </div>
                        </td>';
            }

        $html .='</tr>';
    }
    $html .='</tbody>';
    return $html;
}

function allPermissionDetails(){
    $permissions = 	array(
        'user'=> array(
                        'name'=>'User',
                        'page_link' => '/users',
                        'url_friendly' => 'users',
                        'permission' => 'Member which those run this website',
                        'options' => array('view','update','create','delete')),
        'job'=> array('page_link' => '/jobs',
                        'name'=>'Jobs',
                        'url_friendly' => 'jobs',
                        'permission' => 'jobs permission',
                        'options' => array('view','update','create','delete')),
        'team'=> array(
                        'name'=>'Team',
                        'page_link' => '/team',
                        'url_friendly' => 'team',
                        'permission' => 'team permission',
                        'options' => array('view','update','create','delete')),
        'service'=> array(
                        'name'=>'Service',
                        'page_link' => '/service',
                        'url_friendly' => 'service',
                        'permission' => 'service permission',
                        'options' => array('view','update','create','delete'))
        );

    return $permissions;
}
