<?php

use \Firebase\JWT\JWT;
use Hashids\Hashids;
use App\User;
use Carbon\Carbon;
use Twilio\TwiML\Voice\Client;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\VarDumper\Cloner\Data;

function validate($data)
{
    $message = "";
    foreach ($data as  $key => $value) {
        $message .= $value;
        break;
    }
    return $message;
}
function roundOff($amount)
{
    return number_format($amount, 2, '.', '');
}
function roundOffInt($amount)
{
    return json_decode($amount);
}
function encode($id)
{
    return $id;
    // $hashids = new Hashids('SAAS', 20);
    // return  $hashids->encode($id); // o2fXhV

}


function distance($lat1, $lng1, $lat2, $lng2)
{

    $latitudeFrom = $lat1;
    $longitudeFrom = $lng1;

    $latitudeTo = $lat2;
    $longitudeTo = $lng2;

    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;

    // return $distance *  60 * 1.1515;
    return ($miles);
}

function send_sms($number, $message)
{
    $sid = 'AC5953d205dd2523921f1f6449335ef8b8';
    $token = '2fd056efb7a9989c550ef37078b401a0';
    $client = new Client($sid, $token);

    $otp = mt_rand(100000, 999999);

    $send_otp = $client->messages->create(
        '+917982124464',
        [
            'from' => '+17249135068',
            'body' => 'OTP: ' . $otp
        ]
    );
}


function current_date()
{
    $dt = Carbon::now();
    return $dt->toDateString();
}

function future_date($current_date, $days)
{
    $date = $current_date;
    $addDay = strtotime($date . "+" . $days . " days");

    return date('Y-m-d', $addDay);
}


function decode($e_id)
{
    return $e_id;

    // $hashids = new Hashids('SAAS', 20);
    // $decode = $hashids->decode($e_id);
    // if(sizeof($decode) > 0){
    //     return $decode[0];
    // }
    // return 0;
}

function encryptString($q)
{
    $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
    $qEncoded      = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
    return ($qEncoded);
}

function decryptString($q)
{
    $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
    $qDecoded      = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
    return ($qDecoded);
}

function encCompanyId()
{
    if (auth()->user()) {
        return encode(auth()->user()->companyId());
    } else {
        return 0;
    }
}

function pr($request)
{
    echo "<pre>";
    print_r($request);
    echo "</pre>";
}

function jwtToken()
{
    if (auth()->user() && isset($_COOKIE['api_session']) && strlen($_COOKIE['api_session']) > 0) {
        return $_COOKIE['api_session'];
    }
    return 0;
}
function servierError($msg)
{
    if (env('APP_DEBUG') == true) {
        return $msg;
    }
    return 'Server Error';
}
function generateJwtToken($user, $for_web = 1)
{
    $key = "JWT_TOKEN";
    $payload = array(
        "id" => $user->id,
        "web" => $for_web,
        "app" => !$for_web,
    );
    $jwt = JWT::encode($payload, $key);
    return $jwt;
}

function decodeJwtToken($token, $in_array = 0)
{
    $key = "JWT_TOKEN";
    $decoded = JWT::decode($token, $key, array('HS256'));
    return $decoded;
}

function decodeIds($data)
{
    $final_value = $data;
    if (is_array($data)) {
        $final_value = [];
        foreach ($data as $d) {
            $final_value[] = decode($d);
        }
    }
    return $final_value;
}

// function makeIdsEncode($data){

//     $final_data = new \stdClass;
//     $except_key = ['stirpe_customer_id'];
//     try{
//         if(!is_array($data)){
//             $data = $data->toArray();
//         }
//         foreach($data as $key=>$value){
//             if(is_array($value)){
//                 if(checkExists($value)){
//                     if(array_keys($value) !== range(0, count($value) - 1)){
//                         $final_data->$key = makeIdsEncode($value);
//                     }else{
//                         foreach($value as $array_data){
//                             if(is_array($array_data) || is_object($array_data)){
//                                 $final_data->$key[] = makeIdsEncode($array_data);
//                             }else{
//                                 $final_data->$key[] = $array_data;
//                             }
//                         }
//                     }
//                 }else{
//                     $final_data->$key = [];
//                 }
//             }else{
//                 if((strtolower(substr($key,-3)) == '_id' || $key  == 'id') && is_numeric($value) && !in_array($key, $except_key)){
//                     $new_key = 'enc_'.$key;
//                     $final_data->$new_key = encode($value);
//                 }else{
//                     $final_data->$key = $value;
//                 }
//             }
//         }
//         return $final_data;
//     }catch (\Exception $e) {
//         return $final_data;
//     }
// }

function makeIdsEncode($data)
{
    $final_data = new \stdClass;
    try {
        if (!is_array($data)) {
            $data = $data->toArray();
        }
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (checkExists($value)) {
                    if (array_keys($value) !== range(0, count($value) - 1)) {
                        $final_data->$key = makeIdsEncode($value);
                    } else {
                        foreach ($value as $array_data) {
                            if (is_array($array_data) || is_object($array_data)) {
                                $final_data->$key[] = makeIdsEncode($array_data);
                            } else {
                                $final_data->$key[] = $array_data;
                            }
                        }
                    }
                } else {
                    $final_data->$key = [];
                }
            } else {
                if ((strtolower(substr($key, -3)) == '_id' || $key  == 'id') && is_numeric($value)) {
                    $new_key = 'enc_' . $key;
                    $final_data->$new_key = encode($value);
                } else {
                    $final_data->$key = $value;
                }
            }
        }
        return $final_data;
    } catch (\Exception $e) {
        return $final_data;
    }
}
function checkExists($val = '')
{
    //added this as we using to initialize object with new stdClass from  this getNewObject(), thus when that empty object is passed for empty it return true(i.e exists value), so in order to avoid that mistake, just convert object to array and then when array will be check against for empty then it will return false(i.e no value exists)

    if (is_object($val) && in_array(strval(get_class($val)), array('stdClass')))
        $val = get_object_vars($val);

    if (is_string($val))
        $val = trim($val);

    if (empty($val) || is_null($val))
        return false;
    else
        return true;
}
function unique($data)
{
    return array_values(array_unique((array) $data));
}

function removeValue($array, $value)
{
    $final_array = [];
    for ($i = 0; $i < sizeof($array); $i++) {
        if (checkExists($array[$i]) && $array[$i] != $value) {
            $final_array[] = $array[$i];
        }
    }
    return unique($final_array);
}

function getObj()
{
    return new \stdClass;
}
function s3_upload($request, $key = '', $path = '')
{

    $file = $request->file($key);
    if ($path != '') {
        $fileName = $path . '/' . time() . '.' . $file->getClientOriginalExtension();
    } else {
        $fileName = time() . '.' . $file->getClientOriginalExtension();
    }
    $t = Storage::disk('s3')->put($fileName, file_get_contents($file), 'public');
    $imageName = Storage::disk('s3')->url($fileName);

    return $imageName;
}

function commonToken()
{
    $common_token = \Carbon\Carbon::now()->format("YmdHis");
    return $common_token;
}
function requiredValues($input, $values)
{
    for ($i = 0; $i < sizeof($values); $i++) {
        if (!array_key_exists($values[$i], $input)) {
            return implode(' ', explode('_', $values[$i])) . ' field is required.';
        }
    }
    return false;
}
function s3_upload_multiple($request, $fileName)
{

    $files = $request->file($fileName);
    $ab = array();
    foreach ($files as $key => $file) {
        if (!empty($file['document'])) {
            $fileName = 'document_' . $key . time() . '.' . $file['document']->getClientOriginalExtension();
            $t = Storage::disk('s3')->put($fileName, file_get_contents($file['document']), 'public');
            $imageName = Storage::disk('s3')->url($fileName);
            array_push($ab, $fileName);
        }
    }
    return $ab;
}

function upload_base64($base_64)
{

    $data = $base_64;
    $type = isset($data["type"]) ? $data["type"] . '/' : rand(100, 999);
    $image_array_1 = explode(";", $data);
    $image_array_2 = explode(",", $image_array_1[1]);
    $data = base64_decode($image_array_2[1]);
    $imageName = $type . time() . '.png';
    $p = Storage::disk('s3')->put('profile/' . $imageName, $data, 'public');
    $url = Storage::disk('s3')->url('profile/' . $imageName);
    return  $url;
}

function push_notification($device_id, $title, $message, $image = '')
{

    //API URL of FCM
    $url = 'https://fcm.googleapis.com/fcm/send';
    // https://fcm.googleapis.com/fcm/send

    /*api_key available in:
      Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/
    $api_key = env("FIREBASE_API_KEY");



    $notification = array('title' => $title, 'body' => $message);
    $arrayToSend = array('to' => $device_id, 'notification' => $notification);


    //   $fields = array (
    //       'registration_ids' => array (
    //               $device_id
    //       ),
    //       'data' => array (
    //         "title" => $title,
    //         "body" =>  $message,
    //       )
    //   );
    //   if($image!=''){
    //     $fields['data']['image']=$image;
    //   }


    //header includes Content type and api key
    $headers = array(
        // 'Accept: application/json',
        'Content-Type :application/json',
        'Authorization: key=AAAAPgyAiGY:APA91bHKTYdtf3FMMs0fmuAWAAEWUASKyojtlyGeZOr2jvoXU8390P9OmBIlDTxhul9Wq6wHDxtc8PhFrA7CxE2QUAqAeqyfmt-2lgdT_7nDt29uNiciu6cjtpo5C15i2en33H1_cBMk'
    );

    // dd($headers);

    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_POST, true);
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // // curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    // // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    // // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));
    // $result = curl_exec($ch);
    // if ($result === FALSE) {
    //     die('FCM Send Error: ' . curl_error($ch));
    // }
    // curl_close($ch);
    // dd($result, $headers, $arrayToSend);
    // return json_decode($result);



    // Added Today by Dishant
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);
    // curl_setopt($ch, CURLOPT_POST, 1);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));  //Post Fields
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // $headers = [
    //     'X-Apple-Tz: 0',
    //     'X-Apple-Store-Front: 143444,12',
    //     'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    //     'Accept-Encoding: gzip, deflate',
    //     'Accept-Language: en-US,en;q=0.5',
    //     'Cache-Control: no-cache',
    //     'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
    //     'Host: www.example.com',
    //     'Referer: http://www.example.com/index.php', //Your referrer address
    //     'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0',
    //     'X-MicrosoftAjax: Delta=true'
    // ];
    // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // $server_output = curl_exec($ch);
    // curl_close($ch);
    // dd($server_output);
    // die;


    // New Code



    $title = $title;
    $body = $message;
    $img = $image;
    // $image = $_POST['image'];
    // $link = $_POST['link'];

    $data = [
        "notification" => [
            "body"  => $body,
            "title" => $title,
            "image" => $img
        ],
        "priority" =>  "high",
        "data" => [
            "click_action"  =>  "FLUTTER_NOTIFICATION_CLICK",
            "id"            =>  "1",
            "status"        =>  "done",
            "info"          =>  [
                "title"  => $title,
                // "link"   => $link,
                "image"  => $img
            ]
        ],
        "to" => $device_id
    ];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=AAAAPgyAiGY:APA91bHKTYdtf3FMMs0fmuAWAAEWUASKyojtlyGeZOr2jvoXU8390P9OmBIlDTxhul9Wq6wHDxtc8PhFrA7CxE2QUAqAeqyfmt-2lgdT_7nDt29uNiciu6cjtpo5C15i2en33H1_cBMk';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    curl_close($ch);


    return $result;
}