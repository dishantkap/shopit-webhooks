<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return '<!DOCTYPE html>
    <html>
    <head>
        <title>Page not found - 404</title>
    </head>
    <body>
     
     
    The page your looking for is not available
     
    </body>
    </html>';
});

// Route::get('/{any}',function(Request $request,$any){
//  return "404 page not found";
// });
// Route::get('/{any}/{any1}/{any2}/{any3}/{any4}',function(Request $request,$any){
//     return "404 page not found";
//    });
//    Route::get('/{any}/{any1}',function(Request $request,$any){
//     return "404 page not found";
//    });
//    Route::get('/{any}/{any1}/{any2}',function(Request $request,$any){
//     return "404 page not found";
//    });


// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
