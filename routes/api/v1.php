<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\WebhookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/create_webhooks/{user_id}', [WebhookController::class,'index']);
Route::get('/list_webhooks/{id}', [WebhookController::class,'list_webhooks']);
Route::get('/remove_webhooks/{id}/{w_id}', [WebhookController::class,'remove_webhooks']);
Route::get('/remove_all_webhooks/{user_id}', [WebhookController::class,'remove_all_webhooks']);



/**
 * Webhooks List
 * all Endpoints from Database
 * Store Webhook Responses
 */

// https://admin.shopitnow.app/api/v1/webhook/orders/create/63


Route::post('/webhook/orders/create/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/orders/cancelled/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/products/create/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/products/delete/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/products/update/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/orders/fulfilled/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/orders/paid/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/orders/updated/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/orders/delete/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/webhook/orders/partially_fulfilled/{id}', [ProductController::class,'WebhookResponse']);
Route::post('/store_update', [ProductController::class,'storeOrUpdate']);



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
